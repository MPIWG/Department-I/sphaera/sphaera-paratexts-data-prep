# Social layer data preparation

This repository describes the pre-processing of social layer data derived from
the Sphaera project database. The data is analyzed in

> M. Valleriani, C. Sander. 2021. "Paratexts, Printers, and Publishers: Book Production in Social Context.” In M. Valleriani and A. Ottone (eds.), Publishing Sacrobosco’s «De sphaera» in Early Modern Europe. Modes of Material and Scientific Exchange. Cham: Springer Nature.

## Raw data

The raw data for edges, books and other necessary information is published separatly as

> Victoria Beyer, Nana Citron, Manon Gumpert, Gesa Funke, Beate Federau, Olya Nicolaeva, Irina Tautschnig, Malte Vogl, Christoph Sander, Florian Kräutli, Matteo Valleriani (2020). Sphaera Paratexts Data. DARIAH-DE. https://doi.org/10.20375/0000-000c-d91f-e

The notebook is directly loading data from the published repository.

## Interactive notebooks

An interactive version of the notebook can be accessed via the GESIS Binder.

For the pre-processing of the raw social layers data:

[![Binder](https://notebooks.gesis.org/binder/badge_logo.svg)](https://notebooks.gesis.org/binder/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2FMPIWG%2FDepartment-I%2Fsphaera%2Fsphaera-paratexts-data-prep.git/3107aa32936659a1e1b996b381d38758a5f32136?filepath=Create_social_layers_so4_and_so5.ipynb)
File: Create_social_layers_so4_and_so5.ipynb
